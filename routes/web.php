<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('redirect/google', 'Auth\LoginController@redirectToProvider');
Route::get('callback/google', 'Auth\LoginController@handleProviderCallback');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Route::group(['namespace' => 'Admin'], function () {
//     // Controllers Within The "App\Http\Controllers\Admin" Namespace
// });

Route::group(['prefix' => 'vote', 'middleware' => ['web', 'auth']], function($route){
    Route::get('/{id}', function ($id) {
        return App::make('App\Http\Controllers\VoteController')->show($id);
    })->name('vote');

    Route::post('do', 'VoteController@doVote');
});

Route::group(['prefix' => config('backpack.base.route_prefix', 'admin')], function(){
	// Route::auth();
	Route::get('/login', '\Backpack\Base\app\Http\Controllers\Auth\LoginController@showLoginForm')->name('admin-login');
	Route::post('/login', '\Backpack\Base\app\Http\Controllers\Auth\LoginController@login');
	Route::get('/logout', '\Backpack\Base\app\Http\Controllers\Auth\LoginController@logout');
});


Route::group(['namespace' => 'Admin', 'prefix' => config('backpack.base.route_prefix', 'admin'), 'middleware' => ['role:admin']], function () {
    Route::get('dashboard', '\Backpack\Base\app\Http\Controllers\AdminController@dashboard');
	Route::get('/', '\Backpack\Base\app\Http\Controllers\AdminController@redirect');
    \CRUD::resource('vote', 'VoteCrudController');
    \CRUD::resource('voteanswer', 'VoteAnswerCrudController')->with(function(){
    	Route::get('voteanswer/create/{id}', function ($id) {
    		return App::make('App\Http\Controllers\Admin\VoteAnswerCrudController')->add($id);
    	});

    	Route::get('voteanswer/show/{id}', function($id) {
    		return App::make('App\Http\Controllers\Admin\VoteAnswerCrudController')->showVote($id);
    	});
    });
});

  