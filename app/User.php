<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Backpack\CRUD\CrudTrait;

class User extends Authenticatable
{
    use HasApiTokens;
    use Notifiable;
    use CrudTrait;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'google_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Login or Register user based on provider given
     * 
     * @param  \Laravel\Socialite\Contracts\User $user [description]
     * @param  array $authIdentifier [<description>]
     * @return [type]                            [description]
     */
    public static function loginOrRegister(\Laravel\Socialite\Contracts\User $user, $authIdentifier)
    {
        return User::firstOrCreate(array_merge([
            'email' => $user->getEmail(),
            'name'  => $user->getName(),
        ], $authIdentifier));
    }
}
