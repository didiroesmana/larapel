<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\VoteRepository;

class HomeController extends Controller
{
    protected $vote;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(VoteRepository $vote)
    {
        $this->vote = $vote;
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home', ['votes' => $this->vote->getActiveVote()]);
    }
}
