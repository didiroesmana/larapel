<?php

namespace App\Http\Controllers;

use App\Repositories\UserVotesRepository;
use App\Repositories\VoteAnswerRepository;
use App\Repositories\VoteRepository;
use App\User;
use Auth;
use Illuminate\Http\Request;

class VoteController extends Controller
{
    /**
     * @var mixed
     */
    protected $vote;
    /**
     * @var mixed
     */
    protected $voteAnswer;

    /**
     * @var mixed
     */
    protected $userVotes;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(VoteRepository $vote, VoteAnswerRepository $voteAnswer, UserVotesRepository $userVotes)
    {
        $this->vote       = $vote;
        $this->voteAnswer = $voteAnswer;
        $this->userVotes  = $userVotes;
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd($this->vote->getActiveVote()[0]->getUserVotes());
        return view('home', ['votes' => $this->vote->getActiveVote()]);
    }

    /**
     * Show vote info
     * @param  [type] $id             [description]
     * @return [type] [description]
     */
    public function show($id)
    {
        $user = Auth::user();
        $vote = $this->vote->find($id);
        // dd($vote->getUserVotes());
        if (!$this->userVotes->alreadyVote($user, $vote)) {
            return view('vote', ['vote' => $vote]);
        } else {
            return redirect('/home')->with('error', 'Already vote');
        }
    }

    /**
     * Logic for voting
     * @param  Request $request        [description]
     * @return [type]  [description]
     */
    public function doVote(Request $request)
    {
        $vote = $this->vote->find($request->input('vote_id'));
        $user = Auth::user();
        if ($vote) {
            if ($vote->type == \App\Models\Vote::SINGLE_VOTE) {
                $answer = $this->voteAnswer->find($request->input('vote_answer'));

                if (!$this->userVotes->alreadyVote($user, $vote)) {
                    // vote answer exists
                    if ($vote->answers->contains($answer)) {
                        $this->userVotes->addUserVote($user, $vote, $answer);
                        return redirect('/home')->with('success', 'Vote success');
                    } else {
                        return redirect('/home')->with('error', 'Already vote');
                    }
                } else {
                    return redirect('/home')->with('error', 'Already vote');
                }

            } else {
                // dd($request->input());
            }
            // dd($request->input(), $vote->answers->contains($answer), $answer);
        }

        return redirect('/home');
    }
}
