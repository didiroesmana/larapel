<?php

namespace App\Http\Controllers\Admin\Traits;

/**
 * Custom SaveActions Traits
 */
trait SaveActions
{
	/**
     * Redirect to the correct URL, depending on which save action has been selected.
     * @param  [type] $itemId [description]
     * @return [type]         [description]
     */
    public function performSaveAction($itemId = null)
    {
        $saveAction = \Request::input('save_action', config('backpack.crud.default_save_action', 'save_and_back'));
        $itemId = $itemId ? $itemId : \Request::input('id');
        switch ($saveAction) {
            case 'save_and_new':
                $redirectUrl = $this->save_and_new_path ?: $this->crud->route.'/create';
                break;
            case 'save_and_edit':
                $redirectUrl = $this->crud->route.'/'.$itemId.'/edit';
                if (\Request::has('locale')) {
                    $redirectUrl .= '?locale='.\Request::input('locale');
                }
                break;
            case 'save_and_back':
            default:
                $redirectUrl = $this->save_and_back_path?: $this->crud->route;
                break;
        }
        return \Redirect::to($redirectUrl);
    }
}