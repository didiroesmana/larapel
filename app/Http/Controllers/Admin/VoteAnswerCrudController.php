<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\VoteAnswerRequest as StoreRequest;
use App\Http\Requests\VoteAnswerRequest as UpdateRequest;
use App\Http\Controllers\Admin\Traits\SaveActions as Save;
use Backpack\CRUD\app\Http\Controllers\CrudFeatures\SaveActions;

class VoteAnswerCrudController extends CrudController
{
    use SaveActions, Save {
        Save::performSaveAction insteadof SaveActions;
    }

    protected $save_and_new_path  = '';
    protected $save_and_edit_path = '';
    protected $save_and_back_path = '';

    public function setup($id=0)
    {

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\VoteAnswer');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/voteanswer');
        $this->crud->setEntityNameStrings('voteanswer', 'vote_answers');

        if ( !isset($this->data['entry']) && $id != 0) {
            $this->save_and_new_path  = config('backpack.base.route_prefix') . '/voteanswer/add/'.$id;
            $this->save_and_back_path = config('backpack.base.route_prefix') . "/vote/{$id}/edit";
        }

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $this->crud->setFromDb();

        $this->crud->addField(
            [
                'name'  => 'answer',
                'label' => 'Answer',
                'type'  => 'textarea'
            ], 
            'update/create/both'
        );

        $this->crud->addField(
            [
                'name'      => 'vote_id',
                'label'     => 'Vote',
                'type'      => 'select2',
                'entity'    => 'vote',
                'attribute' => 'help',
                'model'     => 'App\Models\Vote'
            ],
            'update/create/both'
        );
        // ------ CRUD FIELDS
        // $this->crud->addField($options, 'update/create/both');
        // $this->crud->addFields($array_of_arrays, 'update/create/both');
        // $this->crud->removeField('name', 'update/create/both');
        // $this->crud->removeFields($array_of_names, 'update/create/both');

        // ------ CRUD COLUMNS
        // $this->crud->addColumn(); // add a single column, at the end of the stack
        // $this->crud->addColumns(); // add multiple columns, at the end of the stack
        // $this->crud->removeColumn('column_name'); // remove a column from the stack
        // $this->crud->removeColumns(['column_name_1', 'column_name_2']); // remove an array of columns from the stack
        // $this->crud->setColumnDetails('column_name', ['attribute' => 'value']); // adjusts the properties of the passed in column (by name)
        // $this->crud->setColumnsDetails(['column_1', 'column_2'], ['attribute' => 'value']);
        $this->crud->addColumn([
            'name'      => 'vote_id',
            'label'     => 'Vote',
            'type'      => 'select',
            'entity'    => 'vote',
            'attribute' => 'help',
            'model'     => 'App\Models\Vote'
        ]);

        // ------ CRUD BUTTONS
        // possible positions: 'beginning' and 'end'; defaults to 'beginning' for the 'line' stack, 'end' for the others;
        // $this->crud->addButton($stack, $name, $type, $content, $position); // add a button; possible types are: view, model_function
        // $this->crud->addButtonFromModelFunction($stack, $name, $model_function_name, $position); // add a button whose HTML is returned by a method in the CRUD model
        // $this->crud->addButtonFromView($stack, $name, $view, $position); // add a button whose HTML is in a view placed at resources\views\vendor\backpack\crud\buttons
        // $this->crud->removeButton($name);
        // $this->crud->removeButtonFromStack($name, $stack);
        // $this->crud->removeAllButtons();
        // $this->crud->removeAllButtonsFromStack('line');

        // ------ CRUD ACCESS
        // $this->crud->allowAccess(['list', 'create', 'update', 'reorder', 'delete']);
        // $this->crud->denyAccess(['list', 'create', 'update', 'reorder', 'delete']);

        // ------ CRUD REORDER
        // $this->crud->enableReorder('label_name', MAX_TREE_LEVEL);
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('reorder');

        // ------ CRUD DETAILS ROW
        // $this->crud->enableDetailsRow();
        // NOTE: you also need to do allow access to the right users: $this->crud->allowAccess('details_row');
        // NOTE: you also need to do overwrite the showDetailsRow($id) method in your EntityCrudController to show whatever you'd like in the details row OR overwrite the views/backpack/crud/details_row.blade.php

        // ------ REVISIONS
        // You also need to use \Venturecraft\Revisionable\RevisionableTrait;
        // Please check out: https://laravel-backpack.readme.io/docs/crud#revisions
        // $this->crud->allowAccess('revisions');

        // ------ AJAX TABLE VIEW
        // Please note the drawbacks of this though:
        // - 1-n and n-n columns are not searchable
        // - date and datetime columns won't be sortable anymore
        // $this->crud->enableAjaxTable();

        // ------ DATATABLE EXPORT BUTTONS
        // Show export to PDF, CSV, XLS and Print buttons on the table view.
        // Does not work well with AJAX datatables.
        // $this->crud->enableExportButtons();

        // ------ ADVANCED QUERIES
        // $this->crud->addClause('active');
        // $this->crud->addClause('type', 'car');
        // $this->crud->addClause('where', 'name', '==', 'car');
        // $this->crud->addClause('whereName', 'car');
        // $this->crud->addClause('whereHas', 'posts', function($query) {
        //     $query->activePosts();
        // });
        // $this->crud->addClause('withoutGlobalScopes');
        // $this->crud->addClause('withoutGlobalScope', VisibleScope::class);
        // $this->crud->with(); // eager load relationships
        // $this->crud->orderBy();
        // $this->crud->groupBy();
        // $this->crud->limit();
    }

    public function store(StoreRequest $request)
    {
        if ($request->input('from_vote')) {
            $vote_id = $request->input('vote_id');
            $this->save_and_new_path  = config('backpack.base.route_prefix') . '/voteanswer/create/'.$vote_id;
            $this->save_and_back_path = config('backpack.base.route_prefix') . "/vote/{$vote_id}/edit";
        }

        // your additional operations before save here
        $redirect_location = parent::storeCrud();

        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud();
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return $redirect_location;
    }

    public function add($id=0)
    {
        $this->setup($id);
        $this->crud->addField(['name' => 'vote_id', 'type'=> 'hidden', 'value' => $id], 'update/create/both');
        $this->crud->addField(['name' => 'from_vote', 'type'=> 'hidden', 'value' => 1], 'update/create/both');
        return $this->create();
    }

    public function showVote($id=0)
    {
        $this->setup($id);
        $this->crud->addClause('where', 'vote_id', '=', $id);
        return $this->index();
    }
}
