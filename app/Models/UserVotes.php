<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserVotes extends Model
{
    /**
     * @var string
     */
    protected $table = 'user_votes';

    /**
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return mixed
     */
    public function vote()
    {
        return $this->belongsTo('App\Models\Vote');
    }

    /**
     * @return mixed
     */
    public function voteAnswer()
    {
        return $this->belongsTo('App\Models\VoteAnswer', 'answer_id', 'id');
    }
}
