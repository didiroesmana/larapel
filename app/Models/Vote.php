<?php

namespace App\Models;

use Auth;
use Backpack\CRUD\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vote extends Model
{
    use CrudTrait, SoftDeletes;

    const SINGLE_VOTE         = 0;
    const MULTIPLE_VOTE       = 1;
    const SINGLE_VOTE_LABEL   = 'Single Vote';
    const MULTIPLE_VOTE_LABEL = 'Multiple Vote';
    const VOTE_ACTIVE         = 1;
    const VOTE_DISABLED       = 0;
    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
     */

    /**
     * @var string
     */
    protected $table = 'votes';
    //protected $primaryKey = 'id';
    // public $timestamps = false;
    // protected $guarded = ['id'];
    /**
     * @var array
     */
    protected $fillable = ['question', 'active', 'type', 'help'];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
     */
    /**
     * Get the answers for the vote.
     * @return [type] [description]
     */
    public function answers()
    {
        return $this->hasMany('App\Models\VoteAnswer');
    }

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
     */

    /**
     * @return mixed
     */
    public function userVotes()
    {

        return $this->belongsToMany('App\Models\VoteAnswer', 'user_votes', 'vote_id', 'answer_id');
    }

    /**
     * @param  $user
     * @return mixed
     */
    public function getUserVotes($user = null)
    {
        $user = $user ?: Auth::user();
        return $this->userVotes()->where('user_id', $user->id)->get();
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
     */

    /**
     * Scope a query to only include active Vote.
     *
     * @param  \Illuminate\Database\Eloquent\Builder   $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESORS
    |--------------------------------------------------------------------------
     */

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
     */

    /**
     * [getType description]
     * @return array [description]
     */
    public static function getType()
    {
        return [
            self::SINGLE_VOTE   => self::SINGLE_VOTE_LABEL,
            self::MULTIPLE_VOTE => self::MULTIPLE_VOTE_LABEL,
        ];
    }

    public static function getActive()
    {
        return [
            self::VOTE_ACTIVE   => 'Active',
            self::VOTE_DISABLED => 'Vote Disabled',
        ];
    }
}
