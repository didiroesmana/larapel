<?php

namespace App\Repositories;

use App\Models\Vote;

class VoteRepository
{
	protected $vote;

	/**
	 * [__construct description]
	 * @param Post $vote [description]
	 */
	public function __construct(Vote $vote)
	{
	    $this->vote = $vote;
	}

	/**
	 * Get single vote by its id
	 * @param  int $id [description]
	 * @return Vote     [description]
	 */
	public function find($id)
	{
		return $this->vote->find($id);
	}

	/**
	 * Get vote collections by its attribute
	 * @param  string $att    [description]
	 * @param  string $column [description]
	 * @return [type]         [description]
	 */
	public function findBy($att, $column)
	{
		return $this->vote->where($att, $column)->get();
	}

	public function getActiveVote()
	{
		return Vote::active()->get();
	}
}