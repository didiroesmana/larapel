<?php

namespace App\Repositories;

use App\Models\UserVotes;
use App\Models\Vote;
use App\Models\VoteAnswer;
use App\User;

class UserVotesRepository
{
    /**
     * @var mixed
     */
    protected $voteAnswer;
    /**
     * @var mixed
     */
    protected $userVotes;

    /**
     * [__construct description]
     * @param Post $vote [description]
     */
    public function __construct(VoteAnswer $voteAnswer, UserVotes $userVotes)
    {
        $this->voteAnswer = $voteAnswer;
        $this->userVotes  = $userVotes;
    }

    /**
     * @param  User       $user
     * @param  Vote       $vote
     * @param  VoteAnswer $answer
     * @return mixed
     */
    public function addUserVote(User $user, Vote $vote, VoteAnswer $answer)
    {
        $userVoteModel            = new UserVotes();
        $userVoteModel->user_id   = $user->id;
        $userVoteModel->vote_id   = $vote->id;
        $userVoteModel->answer_id = $answer->id;

        return $userVoteModel->save();
    }

    /**
     * @param User $user
     * @param Vote $vote
     * @return mixed
     */
    public function alreadyVote(User $user, Vote $vote)
    {
        return $this->userVotes->where(['user_id' => $user->id, 'vote_id' => $vote->id])->first();
    }
}
