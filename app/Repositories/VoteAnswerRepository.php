<?php

namespace App\Repositories;

use App\Models\VoteAnswer;

class VoteAnswerRepository
{
	protected $voteAnswer;

	/**
	 * [__construct description]
	 * @param Post $vote [description]
	 */
	public function __construct(VoteAnswer $voteAnswer)
	{
	    $this->voteAnswer = $voteAnswer;
	}

	/**
	 * Get single answer by its id
	 * @param  int $id [description]
	 * @return Vote     [description]
	 */
	public function find($id)
	{
		return $this->voteAnswer->find($id);
	}

	/**
	 * Get answers collection by its attribute
	 * @param  string $att    [description]
	 * @param  string $column [description]
	 * @return [type]         [description]
	 */
	public function findBy($att, $column)
	{
		return $this->voteAnswer->where($att, $column)->get();
	}

	/**
	 * Get answers of vote
	 * @param  [type] $id [description]
	 * @return [type]     [description]
	 */
	public function getAnswerOfVote($id)
	{
		return AnswerVote::ofVote($id)->get();
	}
}