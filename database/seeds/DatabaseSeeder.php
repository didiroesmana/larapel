<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('users')->insert([
        	'name' 		=> 'tomcat',
        	'email' 	=> 'tomcat@tomcat.com',
 			'password' 	=> bcrypt('tomcat');       	
        ]);

        $user = User::where('email','tomcat@tomcat.com')->first();

        $role = Role::create(['name'] => 'admin');

        $user->assignRole('admin');
    }
}
