@if (Auth::check())
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="https://placehold.it/160x160/00a65a/ffffff/&text={{ mb_substr(Auth::user()->name, 0, 1) }}" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p>{{ Auth::user()->name }}</p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          <li class="header">{{ trans('backpack::base.administration') }}</li>
          <!-- ================================================ -->
          <!-- ==== Recommended place for admin menu items ==== -->
          <!-- ================================================ -->
          
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>
          <li><a href="{{ url('admin/menu-item') }}"><i class="fa fa-list"></i> <span>Menu</span></a></li>

          @if ($menu_items->count())
              @foreach ($menu_items as $k => $menu_item)
                  @if (($menu_item->page_id && is_object($menu_item->page)) || !$menu_item->page_id)
                    @if ($menu_item->children)
                      <li class="treeview {{ ($k==0)?' fistitem':'' }}">
                        <a href="#">{{ $menu_item->name }} <i class="fa fa-angle-left pull-right"></i></a>
                        <ul class="treeview-menu">
                          @foreach ($menu_item->children as $i => $child)
                            <li class="{{ ($child->url() == Request::url())?'active':'' }}"><a href="{{ $child->url() }}">{{ $child->name }}</a>
                            </li>
                          @endforeach
                        </ul>
                      </li>
                    @else
                      <li class="navitem {{ ($k==0)?' fistitem':'' }} {{ ($menu_item->url() == Request::url())?' active':'' }}">
                          <a href="{{ $menu_item->url() }}">{{ $menu_item->name }}</a>
                      </li>
                    @endif
                  @endif
              @endforeach
          @endif
          <!-- ======================================= -->
          <!-- <li class="header">{{ trans('backpack::base.user') }}</li> -->
          <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/logout') }}"><i class="fa fa-sign-out"></i> <span>{{ trans('backpack::base.logout') }}</span></a></li>
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>
@endif
