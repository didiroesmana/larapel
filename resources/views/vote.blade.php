@extends('layouts.app')

@section('header-content')
    <style>
    .panel-body:not(.two-col) {
        padding: 0px;
    }

    .glyphicon {
        margin-right: 5px;
    }

    .glyphicon-new-window {
        margin-left: 5px;
    }

    .panel-body .radio, .panel-body .checkbox {
        margin-top: 0px;
        margin-bottom: 0px;
    }

    .panel-body .list-group {
        margin-bottom: 0;
    }

    .margin-bottom-none {
        margin-bottom: 0;
    }

    .panel-body .radio label, .panel-body .checkbox label {
        display: block;
    }

    .panel-body .vote-question {
        padding-left: 20px;
        padding-right: 20px;
        padding-top: 10px;
        padding-bottom: 10px;
    }
</style>
@endsection

@section('content')
<div class="container vote">
    <div class="col-md-4"></div>

    <div class="col-md-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><span class="glyphicon glyphicon-th-large"></span> {!! $vote->help !!}</h3>
            </div>
            <div class="panel-body">
                {!! Form::open(['url' => '/vote/do']) !!}
                {!! Form::hidden('vote_id', $vote->id) !!}
                <div class="vote-question">
                    {!! $vote->question !!}
                </div>
                @php
                    $class = $vote->type != App\Models\Vote::SINGLE_VOTE ? 'checkbox' : 'radio';
                @endphp
                <ul class="list-group">
                    @foreach ($vote->answers as $answer)
                        <li class="list-group-item">
                            <div class="{{ $class }}">
                                <label>
                                    @if ($vote->type != App\Models\Vote::SINGLE_VOTE)
                                        <input name="vote_answer[]" type="checkbox" value="{{$answer->id}}"> {{ $answer->answer }}
                                    @else
                                        <input name="vote_answer" type="radio" value="{{$answer->id}}"> {{ $answer->answer }}
                                    @endif
                                </label>
                            </div>
                        </li>
                    @endforeach
                </ul>
                <div class="panel-footer text-center">
                    <input type="submit" class="btn btn-primary btn-block btn-sm" value="Vote">
                </div>
            </div>
            <div class="panel-footer text-center">
                <a href="#" class="small">View Result</a>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
