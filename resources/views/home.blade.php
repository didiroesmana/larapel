@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            @if (session('success'))
                <div class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ session('success') }}
                </div>
            @endif

            @if (session('error'))
                <div class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ session('error') }}
                </div>
            @endif

            <div class="panel panel-info">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in as {{ Auth::user()->name }} !
                </div>
            </div>

            <div class="panel panel-success">
                <div class="panel-heading">Vote</div>

                <div class="panel-body">
                    @if ($votes->count() >= 1)
                        We have {{ $votes->count() }} vote(s) available
                    @else
                        Currently no vote available
                    @endif
                </div>

                @if ($votes->count() >= 1)
                    <ul class="list-group">
                        @foreach ($votes as $vote)
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-md-6">
                                        <a href="{{ route('vote', $vote->id) }}">
                                            {{ $vote->help }}
                                        </a>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        @if (!$vote->getUserVotes()->isEmpty())
                                            Already Vote, nice job <i class="glyphicon glyphicon-thumbs-up"></i>
                                        @endif
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
